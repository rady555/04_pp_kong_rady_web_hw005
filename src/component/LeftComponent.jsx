
import React from 'react'
import category from '../Images/Images/category_icon.png'
import cube from '../Images/Images/cube.png'
import list from '../Images/Images/list.png'
import messenger from '../Images/Images/messenger.png'
import success from '../Images/Images/success.png'
import security from '../Images/Images/security.png'
import users from '../Images/Images/users.png'
import lachlan from '../Images/Images/lachlan.jpg'
import raamin from '../Images/Images/raamin.jpg'
import nonamesontheway from '../Images/Images/nonamesontheway.jpg'
import plus from '../Images/Images/plus.png'

export default function LeftNav() {
  return (
    <div className=' bg-blue-50 h-full flex-row justify-center '>
        <div className='ml-10 pt-5 '>

            <div className='w-6 '>
               <img src={category} alt="error" className='object-scale-down'/>
            </div>
            <div className='w-6 mt-14'>
               <img src={cube} alt="error" className='object-scale-down'/>
            </div>
            <div className='w-6 pt-5'>
              <span class="relative flex h-2 w-2 float-right ">
                <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-red-400 opacity-75 mt-2"></span>
                <span class="relative inline-flex rounded-full h-2 w-2 bg-red-500 mt-2"></span>
              </span>
               <img src={list} alt="error" className=''/>
            </div>
            <div className='w-6 pt-5'>
            <span class="relative flex h-2 w-2 float-right ">
                <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-red-400 opacity-75 mt-2"></span>
                <span class="relative inline-flex rounded-full h-2 w-2 bg-red-500 mt-2"></span>
              </span>
               <img src={messenger} alt="error" className='object-scale-down'/>
            </div>
            <div className='w-6 pt-5'>
               <img src={list} alt="error" className='object-scale-down'/>
            </div>

            <div className='w-6 mt-14'>
               <img src={success} alt="error" className='object-scale-down'/>
            </div>
            <div className='w-6 pt-5'>
               <img src={security} alt="error" className='object-scale-down'/>
            </div>
            <div className='w-6 pt-5'>
               <img src={users} alt="error" className='object-scale-down'/>
            </div>

            <div className='w-9 mt-14 '>
               <img src={lachlan} alt="error" className='w-9 h-9 rounded-full'/>
            </div>
            <div className='w-9 pt-5'>
               <img src={raamin} alt="error" className='w-9 h-9 rounded-full'/>
            </div>
            <div className='w-9 pt-5'>
               <img src={nonamesontheway} alt="error" className=' w-9 h-9 rounded-full '/>
            </div>
            <div className='w-9 pt-5'>
               <img src={plus} alt="error" className='w-9 h-9 rounded-full'/>
            </div>
        </div>
    </div>
  )
}
