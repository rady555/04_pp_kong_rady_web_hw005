
import React, { useState } from 'react'
import Popup from 'reactjs-popup'
import 'reactjs-popup/dist/index.css'

export default function MiddleComponent({data,setData}) {
    // console.log(data);
    const[newState, setNewState] = useState({})
    const [showDetails,setShowDetails] = useState({})
    // functio to get id
        let getId = (id)=>{
            // console.log(id);
            data.map((e)=>{
            if(e.id == id){
            e.status = e.status === 'beach'? 'forest' : e.status === 'forest'? 'mountain' : 'beach';
            }
            setData([...data])
            })
        }

        // function to check id
        let checkId = (id)=>{
            // console.log(id);
            data.map((e)=>{
                if(e.id == id){
                    setShowDetails({
                    title : e.title,
                    description : e.description,
                    peopleGoing : e.peopleGoing
                })
            }
                setData([...data])
            })
        }

        // function handle change
        let handleChange = (e) => {
            const {name,value} = e.target;
            setNewState({...newState, [name]:value});
        }
    // function handle submit
        let handleSubmit = (e) => {
            e.preventDefault();
            console.log(newState);
            setData([...data, {...newState, id: data.length +1}]);
        }

  return (
        <div className='p-5 font-Josefin mt-10 '>
            <div className=' grid grid-cols-2'>
                <div className='w-4/5 font-semibold col-span-1'>
                    <h1 className='text-3xl text-left text-black'> Good Evening Team! </h1>
                </div>
                <div className='col-span-1'>
                    <div className='float-right mr-7'>
                        {/* The button to add new trip */}
                        <label htmlFor="my-modal-5" className="btn text-white">add new trip</label>
                        <input type="checkbox" id="my-modal-5" className="modal-toggle " />
                        <div className="modal ">
                            <div className="modal-box relative bg-white text-black">
                                <label htmlFor="my-modal-5" className="btn btn-sm btn-circle  absolute right-2 top-2">✕</label>
                                <div className=''>
                                    {/* form add new data */}
                                        <form action="" className='text-lg'>
                                            <div className="form-control w-full text-black ml-2 max-w-md">
                                                <label className="label p-0 pt-2">
                                                    <span className="label-text text-black text-lg">Title</span>
                                                </label>
                                                <input type="text" name='title' onChange={handleChange} placeholder="Sihaknou Ville" className="bg-white input input-bordered input-secondary w-full max-w-md" />
                                                <label className="label p-0 pt-2">
                                                    <span className="label-text text-black text-lg">Description</span>
                                                </label>
                                                <input type="text" name='description' onChange={handleChange} placeholder="Happy place with beautiful beach" className="bg-white input input-bordered input-secondary w-full max-w-md" />
                                                <label className="label p-0 pt-2">
                                                    <span className="label-text text-black text-lg">People going</span>
                                                </label>
                                                <input type="text" name='peopleGoing' onChange={handleChange} placeholder="3200" className="bg-white input input-bordered input-secondary w-full max-w-md" />
                                                <label className="label p-0 pt-2">
                                                    <span className="label-text text-black text-lg">Type of adventure</span>
                                                </label>
                                                <select name='status' onChange={handleChange} className="bg-white input input-bordered input-secondary w-full max-w-md">
                                                    <option disabled selected>----Choose any option----</option>
                                                    <option value="beach">Beach</option>
                                                    <option value="forest">Forest</option>
                                                    <option value="mountain">Mountain</option>
                                                </select> 
                                            </div>
                                            <button className="btn btn-primary mt-5 px-5 float-left ml-3" onClick={handleSubmit}>Submit</button>                              
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

           {/* card */}
            <div className='flex flex-wrap gap-3 my-5'>  
            {data.map((items, index) => (
                <div class=" col-span-2 max-w-xs p-6 text-white border border-gray-200 rounded-2xl shadow text-left bg-gray-800">
                    <a href="#">
                        <h5 class="mb-2 text-2xl font-normal tracking-tight  uppercase" >{items.title === undefined?'null':items.title}</h5>
                    </a>
                    <p id="truncate" class="mb-3 font-normal text-ellipsis overflow-hidden line-clamp-3 "> {items.description === undefined?'null': items.description}</p>
                    <p class="mb-3 font-normal  ">People going <br></br><span className='text-2xl'>{items.peopleGoing === undefined?'null' : items.peopleGoing}</span></p>
                    
                    <div className='grid grid-cols-2 gap-2'>
                         <button onClick={()=>getId(items.id)} className={items.status === "beach"?"uppercase rounded-md bg-blue-500 hover:bg-blue-600" : items.status === "mountain"?" uppercase rounded-md bg-gray-600 hover:bg-gray-600" :" uppercase rounded-md bg-green-500 hover:bg-green-600"}>                        
                            {items.status === undefined?'null' :items.status}
                        </button>                  
                        <div>
                            <div>
                                {/* button click to show detail */}
                                <label onClick={()=>checkId(items.id)} htmlFor="my-modal-3" className="btn text-white font-semibold w-36">Read Details</label>
                                <input type="checkbox" id="my-modal-3" className="modal-toggle" />               
                                <div className="modal text-black ">
                                    <div className="modal-box relative bg-white">
                                        <label htmlFor="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                                        <h3 className="text-lg font-bold uppercase">{showDetails.title === undefined?'null':items.title }</h3>
                                        <p className="py-4">{showDetails.description === undefined?'null': items.description}</p>
                                        <p>Around <span>{showDetails.peopleGoing === undefined?'null' : items.peopleGoing}</span> people going there</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>                
             )) }
            </div>
        </div>
    
  )
}
