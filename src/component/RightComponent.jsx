
import React from 'react'
import notification from '../Images/Images/notification.png'
import comment from '../Images/Images/comment.png'
import lachlan from '../Images/Images/lachlan.jpg'
import raamin from '../Images/Images/raamin.jpg'
import nonamesontheway from '../Images/Images/nonamesontheway.jpg'
import christina from '../Images/Images/christina.jpg'


export default function RightComponent() {
  return (
    
        <div className=' bg-image w-full bg-cover h-screen p-4 font-Josefin'>
            <div className=' '>
                <div className=' mt-5 w-full h-20'>
                  <div className='flex gap-4 float-right mr-10 justify-center items-center'>
                    <img src={notification} alt="error" className='w-8 h-8'/>
                    <img src={comment} alt="error" className='w-8 h-8'/>
                    <img src={lachlan} alt="error" className='w-10 h-10 rounded-full'/>
                  </div>
                </div>
                <div className=' w-full h-14'>
                  <button className='float-right text-black bg-yellow-200 rounded-lg py-2 px-3 mr-5'>My amazing trip</button>
                </div>
                <div className=''>
                  <p className='text-white text-3xl text-left'> I like lying down on the sand and looking at the moon.</p>
                </div>
                <div className='mt-10 '>
                  <div>
                    <p className='text-left text-xl text-white'>27 people going to this trip</p>
                  </div>
                  <div className='mt-5 flex justify-center'>
                    <div className="avatar mx-3">
                      <div className="w-12 rounded-full ">
                        <img src={lachlan} className=""/>
                      </div>
                    </div>
                    <div className="avatar mx-3">
                      <div className="w-12 rounded-full ring ring-white ring-offset-base-100 ring-offset-2">
                        <img src={raamin} />
                      </div>
                    </div>
                    <div className="avatar mx-3">
                      <div className="w-12 rounded-full ring ring-error ring-offset-base-100 ring-offset-2">
                        <img src={nonamesontheway} />
                      </div>
                    </div>
                    <div className="avatar mx-3">
                      <div className=" w-12 rounded-full ring ring-white ring-offset-base-100 ring-offset-2">
                        <img src={christina} />
                      </div>
                    </div>
                    <div className="avatar mx-3 w-12 h-12 rounded-full bg-orange-200 outline-dashed outline-orange-400 justify-center items-center ">
                      23+ 
                    </div>
                  </div>
                </div>
            </div>
        </div>
    
  )
}
