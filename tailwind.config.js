/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily:{
        'Josefin' : 'Josefin Sans, sans-serif;',
      },
      backgroundImage: {
        'image': "url('/src/Images/Images/bg.jpg')",
      }
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    require("daisyui"),
  ],

}
